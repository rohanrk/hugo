---
title: El Comedor Social
comments: false
---

![](../images/comedor.jpg)

El Comedor Social es un comedor comunitario que está abierto todos los días desde doce y media por la tarde hasta una y media. Empezamos preparar la cocina más o menos a las doce. Servimos un plato principal con un segundo plato y una guarnición

Nunca tenemos cerdo en nuestra comida porque hay gente que no comerá cerdo por razones religiosas. 

![](../images/tornillo.jpg)

Pasta de tornillo

![](../images/jugo-salir.jpg)

Siempre ofrecemos una bolsa de comida para llevar. Incluye zumo, fruta y/o dulces.