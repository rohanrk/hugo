---
title: Las directoras
subtitle: No podría hacerlo sin ellas
comments: false
---

![Inés](../images/Ines.jpeg)

Inés fue la primera persona que conocí en el hospital. Ella nos. Ella se apaña todos los papeleos y las logísticas y promociona el programa social del hospital a voluntarios nuevos. Maneja muchos programas incluyendo el banco de alimentación y la donación de ropa. Usualmente ayuda por la tarde con la recepción o con el reciclaje de los cartones. Ella me ayudó mucho con mis asignaturas de la clase de servicio y siempre participó en nuestras entrevistas, encuestas y actividades.

![Luisa](../images/Luisa.jpg)

Veo a Luisa durante la mañana. Ella ayuda preparar las bolsas para la tarde y maneja el almacén de comida. Después de terminar las bolsas, ella cuenta los suministros, asegura que tenemos suficientes cosas para las semanas siguientes y pide suministros que necesitamos.