---
title: El Banco de Alimentación
subtitle: Entrenamiento para el supermercado
comments: false
---

Hay dos partes del trabajo en el banco de alimentación. Por la mañana, recibimos las listas con toda la comida que las familias necesitan. Cada familia recibe un número. Colocamos toda la comida por la mañana y preparamos las bolsas de comida para la tarde.

Por la mañana, preparamos las bolsas con la comida que las familias nos han pedido. Después de terminar, preparamos bolsas de fruta y cosas extras como yogur o flan para la tarde. 

Vé [aquí](../comida) para ver qué tenemos.

![](../images/Cosas.jpg "Un día normal")

![](../images/Fruta.jpg)

Por la tarde, las familias llegan para recibir su comida. Hay una recepción donde alguien apunta el nombre de la familia y nos dice el número que necesita. Cada familia recibe una bolsa de patatas, frutas, extras y una caja de huevos.

![](../images/cajas.jpg)