---
title: ¿Quién soy?
subtitle: Llevé un speedo una vez
comments: false
---

Me llamo Rohan. Yo soy un estudiante de informática en Georgia Tech que está estudiando en España por dos semestres. Cuando yo estaba en Granada yo...

- He visitado la Alhambra
- He visto un espectáculo de flamenco
- Durmí en una cueva

Este semestre, he participado en un programa de sostenibilidad en Granada. Trabajé en el Hospital San Juan de Dios para alimentar a los pobres. Aquí encontrarás más información sobre mis actividades mientras estaba aquí.

Trabajé en el [comedor social](../comedor) y el [banco de alimentación](../banco)

![](../images/me.jpg)