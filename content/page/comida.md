---
title: ¡Bienvenidos!
subtitle: Tenemos todo que se necesita
comments: false
---

### Vea a nuestra almacén

Arroz
![](../images/comida/arroz.jpg)

Atún
![](../images/comida/atun.jpg)

Caldo de pollo
![](../images/comida/caldo-pollo.jpg)

Garbanzo
![](../images/comida/garbanzos.jpg)

Macarones
![](../images/comida/macarones.jpg)

Tomate frito
![](../images/comida/tomate-frito.jpg)

Berenjena
![](../images/comida/berenjena.jpg)

Pimienta
![](../images/comida/pimienta.jpg)

Pepino
![](../images/comida/pepino.jpg)

Leche
![](../images/comida/leche.jpg)

Cacao
![](../images/comida/cacao.jpg)

Yogur
![](../images/comida/yogur.jpg)

### ¡Tenemos más que comida!

Gel y lavajilla
![](../images/comida/lavajilla.jpg)

Detergente
![](../images/comida/detergente.jpg)

### Para los bebés

Pañales
![](../images/comida/panuelos.jpg)

Potitos
![](../images/comida/potitos.jpg)